# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions
activate :dotenv

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

require 'lib/page_mapper'
require 'lib/post_mapper'

activate :contentful do |f|
  f.access_token  = ENV['CONTENTFUL_ACCESS_TOKEN']
  f.space         = { site: ENV['CONTENTFUL_SPACE_ID'] }
  # f.cda_query     = { limit: 1000 }
  # f.content_types = { person: 'person', pages: 'page', posts: 'blogPost' }
  f.content_types = {
    pages: { mapper: PageMapper, id: 'page' },
    posts: { mapper: PostMapper, id: 'blogPost' }
  }
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

require 'lib/markdown_helper'

set :markdown_engine, :redcarpet

helpers do
  # other methods...

  def markdown(text)
    Tilt['markdown'].new(context: @app) { text }.render
  end
end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-setting

configure :build do
  # Set build directory
  set :build_dir, 'public'
  
  # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  # set :base_url, "brittany_garcia.gitlab.io/middleman_site/" 
  set :base_url, ENV['BASE_URL']
  set :relative_links, true


  # Use relative URLs
  activate :relative_assets 

  # Minify css on build
  activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript, ignore: "**/admin/**", compressor: ::Uglifier.new(mangle: true, compress: { drop_console: true }, output: {comments: :none})

  # Use Gzip
  activate :gzip



  #Use asset hashes to use for caching
  #activate :asset_hash

end


# Ignore all templates. (This saves us from ignoring within the loop and
# protects us against an error if one of the data types doesn't exist.)
ignore 'templates/*.html'

# Checks to ensure pages data exists before trying to access it
if @app.data.try(:site).try(:pages)
  # Loop through each page
  data.site.pages.each do |_id, page|
    # The path to the page gets set from the slug of the page
    path = "#{page.slug}/index.html"
    # Use the appropriate template
    # template = "templates/page/home.html"
    template = "templates/page/#{page.template_name.parameterize}.html"
    # Add the proxy
    proxy path, template, locals: { page: page }
  end
end

if @app.data.try(:site).try(:posts)
  data.site.posts.each do |_id, post|
    date = post.publish_date
    path = "blog/#{date.year}-#{'%02i' % date.month}-#{'%02i' % date.day}-#{post.slug}/index.html"
    template = "templates/post.html"
    proxy path, template, locals: { post: post }
  end
end

