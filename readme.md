This repo contains an example website that is built with Middleman and Contentful CMS.

## About the architecture
Middleman is a static site generator using all the shortcuts and tools in modern web development. Check out middlemanapp.com for detailed tutorials, including a getting started guide.

Contentful provides a content infrastructure for digital teams to power content in websites, apps, and devices. Unlike a CMS, Contentful was built to integrate with the modern software stack. It offers a central hub for structured content, powerful management and delivery APIs, and a customizable web app that enable developers and content creators to ship digital products faster.

Contentful Middleman is a Middleman extension to use the Middleman static site generator together with Contentful. It is powered by the Contentful Ruby Gem.

## How to Run

Paste the following command inside your terminal:

```bash
echo "Checkout Repository"         && git clone git@gitlab.com:brittany_garcia/middleman_site.git && \
echo "Install Dependencies"        && bundle install && \
echo "Start Middleman Server"      && bundle exec middleman server 
```

Then open your browser and go to: [localhost:4567](http://localhost:4567)

## Configuration

Sign up for a free contentful account [here](https://www.contentful.com/)

Copy the .env-sample file and rename it to .env, add your unique keys from contentful account.
CONTENTFUL_SPACE_ID=""
CONTENTFUL_ACCESS_TOKEN=""

## Content Modeling

#Post Modeling

Create content model named "post" 
Create a post type content entry, save and publish

#Page Modeling

Create content model named "page" with a entries 
- template_name
- slug

Create, save and publish three page type content entries with the following template names:
contact
event
home

Pull down your data and build.

```bash
echo "Fetch Contentful Data"       && bundle exec middleman contentful && \
echo "Start Middleman Server"      && bundle exec middleman server 
```

## References

Example page and post content is located in the /data/ directory
Example page and post templates are located in the /source/templates/page/ directory
