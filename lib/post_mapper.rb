class PostMapper < ContentfulMiddleman::Mapper::Base
    def map(context, entry)
      super
      date = context.publish_date
      context.path = "/blog/#{date.year}-#{'%02i' % date.month}-#{'%02i' % date.day}-#{context.slug}/"
      context.file_path = "#{context.path}index.html"
      context.template = 'templates/post.html'
      # context.image_url = "https://#{context.hero_image.url}"
      context.unsplash_image = "https://source.unsplash.com/collection/148542/watch-the-sky/"
    end
  end
