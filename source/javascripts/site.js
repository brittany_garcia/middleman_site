// This is where it all goes :)

// parallax refill

$(document).ready(function() {
    if ($("#js-parallax-window").length) {
      parallax();
    }
  });
  
  $(window).scroll(function(e) {
    if ($("#js-parallax-window").length) {
      parallax();
    }
  });
  
  function parallax(){
    if( $("#js-parallax-window").length > 0 ) {
      var plxBackground = $("#js-parallax-background");
      var plxWindow = $("#js-parallax-window");
  
      var plxWindowTopToPageTop = $(plxWindow).offset().top;
      var windowTopToPageTop = $(window).scrollTop();
      var plxWindowTopToWindowTop = plxWindowTopToPageTop - windowTopToPageTop;
  
      var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
      var windowInnerHeight = window.innerHeight;
      var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
      var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
      var plxSpeed = 0.35;
  
      plxBackground.css('top', - (plxWindowTopToWindowTop * plxSpeed) + 'px');
    }
  }

//   nav refill

$(window).on("load resize",function(e) {
    var more = document.getElementById("js-centered-more");
  
    if ($(more).length > 0) {
      var windowWidth = $(window).width();
      var moreLeftSideToPageLeftSide = $(more).offset().left;
      var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;
  
      if (moreLeftSideToPageRightSide < 330) {
        $("#js-centered-more .submenu .submenu").removeClass("fly-out-right");
        $("#js-centered-more .submenu .submenu").addClass("fly-out-left");
      }
  
      if (moreLeftSideToPageRightSide > 330) {
        $("#js-centered-more .submenu .submenu").removeClass("fly-out-left");
        $("#js-centered-more .submenu .submenu").addClass("fly-out-right");
      }
    }
  
    var menuToggle = $("#js-centered-navigation-mobile-menu").unbind();
    $("#js-centered-navigation-menu").removeClass("show");
  
    menuToggle.on("click", function(e) {
      e.preventDefault();
      $("#js-centered-navigation-menu").slideToggle(function(){
        if($("#js-centered-navigation-menu").is(":hidden")) {
          $("#js-centered-navigation-menu").removeAttr("style");
        }
      });
    });
  }); 